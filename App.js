import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "./src/Login";
import Home from "./src/Home";
import Akademik from "./src/Akademik";
import Keuangan from "./src/Keuangan";
import Akun from "./src/Akun"

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home"
        screenOption={{ headerShown: false }}>
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="Home" component={Home}/>
        <Stack.Screen name="Akademik" component={Akademik}/>
        <Stack.Screen name="Keuangan" component={Keuangan}/>
        <Stack.Screen name="Akun" component={Akun}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;