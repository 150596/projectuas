import React, {useState, useEffect} from "react";
import { View, Text, TouchableOpacity, Image, ScrollView, StatusBar } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";


const Keuangan = ({navigation}) => {
  return (
    <View style= {{flex: 1}}>
      <View style={{flex: 1,marginHorizontal: 20, marginTop: 10}}>
        <View style={{marginHorizontal: 30,marginTop: 20}}></View>
      <Image
        style={{width: 80, height: 80,marginLeft:'43%'}}
        source= {require("./images/riy.png")}/>
      <Text style={{color: "#212121",textAlign: "center",fontSize: 20, marginTop: 20}}> Hi, Selamat Datang</Text>
      <Text style={{fontSize: 32,textAlign: "center",fontWeight: "bold", color: "#1E90FF"}}>
      Nadien Olivia 👋</Text>
      <View style={{marginTop: 20,borderRadius: 5,
        backgroundColor: "#000",elevation: 1,
        textAlign: "center", marginRight:20,
        width: 600, height: 50, marginLeft:40}}>
        <Text style={{color: "#FFD700",fontSize: 30, fontWeight: "bold", textAlign: "center"}}> PROFIL SANTRI</Text>
      </View>
      <View style={{borderRadius: 5,
        backgroundColor: "#fff",elevation: 1,
        textAlign: "left", marginRight:20,
        width: 600, height: 300, marginLeft:40,borderWidth: 2}}>
        <Text style={{color: "#000", marginTop: 15, fontSize: 30, fontWeight: "bold", textAlign: "center"}}> Nadien Olivia</Text>
        <Text style={{color: "#808080", marginTop: 15, fontSize: 28, textAlign: "center"}}> NIS : 10027081</Text>
        <Text style={{color: "#808080", fontSize: 28, textAlign: "center"}}> Asal : Sumenep</Text>
        <Text style={{color: "#808080", fontSize: 28, textAlign: "center"}}> Tetala : Sumenep, 18 Juli 2001</Text>
        <Text style={{color: "#808080", fontSize: 28, textAlign: "center"}}> Ayah : Mahfudz</Text>
        <Text style={{color: "#808080", fontSize: 28, textAlign: "center"}}> Ibu : Sulfah</Text>
      </View>
      <View style={{marginTop: 20,borderRadius: 5,
        backgroundColor: "#000",elevation: 1,
        textAlign: "center", marginRight:20,
        width: 600, height: 50, marginLeft:40}}>
        <Text style={{color: "#FFD700",fontSize: 30, fontWeight: "bold", textAlign: "center"}}> KEUANGAN BULANAN</Text>
      </View>
      <View style={{borderRadius: 5,
        backgroundColor: "#fff",elevation: 1,
        textAlign: "center", marginRight:20,
        width: 600, height: 350, marginLeft:40,borderWidth: 2}}>
        <Text style={{color: "#000", marginTop: 15, fontSize: 24, fontWeight: "bold", textAlign: "left"}}> Kamar :</Text>
        <Text style={{color: "#808080", marginTop: 2, fontSize: 18, marginLeft:5}}> Al Humairoh</Text>
        <Text style={{color: "#000", marginTop: 5, fontSize: 24, fontWeight: "bold", textAlign: "left"}}> Ketua Kamar :</Text>
        <Text style={{color: "#808080", marginTop: 2, fontSize: 18, marginLeft:5}}> Ustadzah. Robiatul Fajariyah</Text>
        <Text style={{color: "#000", marginTop: 5, fontSize: 24, fontWeight: "bold", textAlign: "left"}}> Pembayaran Bulan Kemarin :</Text>
        <Text style={{color: "#808080", marginTop: 2, fontSize: 18, marginLeft:5}}> Lunas</Text>
        <Text style={{color: "#000", marginTop: 5, fontSize: 24, fontWeight: "bold", textAlign: "left"}}> Pembayaran Bulan Ini :</Text>
        <Text style={{color: "#808080", marginTop: 2, fontSize: 18, marginLeft:5}}> Lunas</Text>
        <Text style={{color: "#000", marginTop: 5, fontSize: 24, fontWeight: "bold", textAlign: "left"}}> Pembayaran Bulan Depan :</Text>
        <Text style={{color: "#808080", marginTop: 2, fontSize: 18, marginLeft:5}}> Belum Lunas</Text>
      </View>
      <View style={{marginHorizontal: 30,marginTop: 20}}></View>
      <View style= {{flex: 1}}></View>
      <View style={{flexDirection: "row",paddingVertical: 5}}>
        <TouchableOpacity 
        onPress={()=> navigation.navigate('Home')}
        style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
        <Icon name="home" size={35} color="#808080" />
        <Text style={{color: "#808080"}}>Beranda</Text>
        </TouchableOpacity>
        <TouchableOpacity
        onPress={()=> navigation.navigate('Akademik')}
        style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
        <Icon name="school" size={35} color="#808080" />
        <Text>Akademik</Text>
        </TouchableOpacity>
        <TouchableOpacity
        onPress={()=> navigation.navigate('Keuangan')}
        style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
        <Icon name="wallet" size={35} color="#1E90FF" />
        <Text style={{color: "#1E90FF"}}>Keuangan</Text>
        </TouchableOpacity>
        <TouchableOpacity
        onPress={()=> navigation.navigate('Akun')}
        style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
        <Icon name="person" size={35} color="#808080" />
        <Text>Akun</Text>
        </TouchableOpacity>
      </View>
    </View>
    </View>
  );
};

export default Keuangan;