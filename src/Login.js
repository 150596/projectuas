import React, {Component, component} from "react";
import {ScrollView, Text, ImageBackground, TextInput, Image, TouchableOpacity, 
View} from "react-native";
import Home from "./Home";

const Login = ({navigation}) => {
    return (
      <ScrollView>
    <ImageBackground
    source={require("./images/olif.jpg")}
    style={{flex: 1, width: 750, height: 1400}}>
      <Image style={{width: 200, height: 200,marginLeft:250}}
      source= {require ("./images/Alhasyimiyah.png")}/>
    <Text style={{fontSize: 34, textAlign: 'center', fontWeight: 'bold',
    color: "#228B22"}}>PONDOK PESANTREN SUMBER PAYUNG</Text>
    <Text style={{fontSize: 32, textAlign: 'center',color: "#1E90FF",
    fontWeight: 'bold'}}>DAERAH AL HASYIMIYAH</Text>
    <Text style={{fontSize: 26, textAlign: 'center',color: "#000",
    }}>Bataal Barat-Ganding-Sumenep</Text>
    <TextInput
        style={{ borderWidth: 1,
          marginHorizontal: 100, backgroundColor: '#ffffff', marginTop: 300,opacity: 0.8,
          borderRadius: 80, elevation: 4,
          paddingLeft: 20, letterSpacing: 1, paddingTop: 15, paddingBottom: 15
        }} 
        placeholder="Username / NIS" />
    <TextInput
        style={{ borderWidth: 1,
          marginHorizontal: 100, backgroundColor: '#ffffff', marginTop: 40,opacity: 0.8,
          borderRadius: 80, elevation: 4,
          paddingLeft: 20, letterSpacing: 1, paddingTop: 15, paddingBottom: 15
        }}
        placeholder="Password" />
        <View>
        <TouchableOpacity 
        onPress={()=> navigation.navigate('Home')}
        style={{backgroundColor: '#228B22', width: 200, height: 60,
        marginTop: 50, elevation: 3, marginLeft: 400, borderRadius: 20,
         borderColor: '#fff', borderWidth: 2.5}}>
          <Text style={{fontSize: 28, fontWeight: 'bold',
          fontWeight: 'bold', color: '#fff', textAlign: 'center',
          marginTop: 8, }}>Masuk</Text>
        </TouchableOpacity></View>
  </ImageBackground>
  </ScrollView>
  );
  }


export default Login;